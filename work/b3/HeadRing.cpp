#include "HeadRing.h"

THeadRing::THeadRing() : TDatList()
{
    pHead = GetLink();
    pStop = pHead;
    pLast = pFirst = pCurrLink = pStop;
    pLast->SetNextLink(pHead);
}

THeadRing::~THeadRing() {
    DelList();
    DelLink(pHead);
}

void THeadRing::InsFirst(PTDatValue pVal)
{
    TDatList::InsFirst(pVal);
    pHead->SetNextLink(pFirst);
}

void THeadRing::DelFirst()
{
    TDatList::DelFirst();
    pHead->SetNextLink(pFirst);
}