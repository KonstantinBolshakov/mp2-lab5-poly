#include "Polynom.h"
#include <string>
#include <iostream>
#include <map>
#include <functional>

void TPolynom::sort(int monoms[][2], int km)
{
    std::map<int, int, std::greater<int>> tmp;

    for (int i = 0; i < km; i++)
        tmp.insert({ monoms[i][1], monoms[i][0] });
    auto tmp_it = tmp.cbegin();
    for (int i = 0; i < km; i++)
    {
        monoms[i][0] = tmp_it->second;
        monoms[i][1] = tmp_it->first;
        tmp_it++;
    }
}

TPolynom::TPolynom(int monoms[][2], int km) : THeadRing()
{
    if (km < 0)
        throw std::invalid_argument("Negative number");
    sort(monoms, km);
    pHead->SetDatValue(new TMonom(0, -1));
    for (int i = 0; i < km; i++)
        InsLast((PTDatValue)new TMonom(monoms[i][0], monoms[i][1]));
}

TPolynom::TPolynom(const TPolynom &q) :THeadRing()
{
    //pHead->SetDatValue(new TMonom(0, -1));
    PTDatLink tmp = q.pFirst;
    for (int i = 0; i < q.ListLen; i++)
    {
        InsLast((TMonom*)tmp->GetDatValue()->GetCopy());
        tmp = tmp->GetNextDatLink();
    }
}

TPolynom TPolynom::operator+(TPolynom &q)
{
  /*  TPolynom res(*this);
    res.Reset();
    q.Reset();
    while (true)
    {
        auto res_m = res.GetMonom();
        auto q_mon = q.GetMonom();
        if (*res_m < *q_mon)
        {
            res.InsCurrent(q_mon->GetCopy());
            q.GoNext();
        }
        else if (res_m->GetIndex() > q_mon->GetIndex())
            res.GoNext();
        else
        {
            if (res_m->GetIndex() == -1) break;
            auto tmp_coeff = res_m->GetCoeff() + q_mon->GetCoeff();
            if (tmp_coeff == 0)
            {
                res.DelCurrent();
                q.GoNext();
            }
            else
            {
                res_m->SetCoeff(tmp_coeff);
                res.GoNext();
                q.GoNext();
            }
        }
    }
    return res;*/  
    PTMonom pm, qm, tm;
Reset(); q.Reset();
while (true)
{
    pm = GetMonom(); qm = q.GetMonom();
    if (pm->Index < qm->Index)
    {
        tm = new TMonom(qm->Coeff, qm->Index);
        InsCurrent(tm); q.GoNext();
    }
    else if (pm->Index > qm->Index)
        GoNext();
    else
    {
        if (pm->Index == -1)
            break;
        pm->Coeff += qm->Coeff;
        if (pm->Coeff != 0)
        {
            GoNext(); q.GoNext();
        }
        else
        {
            DelCurrent(); q.GoNext();
        }
    }
}
return *this;
}

TPolynom &TPolynom::operator=(const TPolynom &q)
{
    if (&q != this)
    {
        DelList();
        PTDatLink tmp = q.pFirst;
        for (int i = 0; i < q.ListLen; i++)
        {
            InsLast((PTMonom)tmp->GetDatValue()->GetCopy());
            tmp = tmp->GetNextDatLink();
        }
    }
    return *this;
}