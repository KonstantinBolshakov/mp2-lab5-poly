#include <gtest\gtest.h>
#include "Polynom.h"

TEST(DatLink, can_create_link)
{
    ASSERT_NO_THROW(TDatLink dl);
}

TEST(Datlist, can_create_data_list)
{
    ASSERT_NO_THROW(TDatList list);
}

TEST(Datlist, can_get_first_data_value)
{
    TDatList list;
    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    TMonom tmp(1, 131);
    EXPECT_TRUE(tmp == *(TMonom*)list.GetDatValue());
}

TEST(Datlist, can_get_current_data_value)
{
    TDatList list;
    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    TMonom tmp(4, 464);
    list.SetCurrentPos(3);
    EXPECT_TRUE(tmp == *(TMonom*)list.GetDatValue());
}

TEST(Datlist, can_get_list_length)
{
    TDatList list;
    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    EXPECT_EQ(listLen, list.GetListLength());
}

TEST(Datlist, can_get_and_set_current_pos)
{
    TDatList list;
    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    list.SetCurrentPos(3);
    EXPECT_EQ(3, list.GetCurrentPos());
}

TEST(Datlist, cant_set_negative_pos)
{
    TDatList list;
    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    ASSERT_ANY_THROW(list.SetCurrentPos(-3));
}

TEST(Datlist, cant_set_incorrect_pos)
{
    TDatList list;
    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    ASSERT_ANY_THROW(list.SetCurrentPos(11));
}

TEST(Datlist, can_go_next_link)
{
    TDatList list;
    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    list.SetCurrentPos(0);
    ASSERT_NO_THROW(list.GoNext());

    TMonom tmp(2, 242);
    EXPECT_TRUE(tmp == *(TMonom*)list.GetDatValue());
}

TEST(Datlist, cant_go_next_link_if_list_ended)
{
    TDatList list;
    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    list.SetCurrentPos(listLen - 1); list.GoNext();
    EXPECT_EQ(1, list.GoNext());
}

TEST(Datlist, IsListEnded_works_correctly)
{
    TDatList list;
    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    list.SetCurrentPos(2);
    EXPECT_FALSE(list.IsListEnded());

    list.SetCurrentPos(listLen - 1); list.GoNext();
    EXPECT_TRUE(list.IsListEnded());
}

TEST(Datlist, IsEmpty_works_correctly)
{
    TDatList list;
    EXPECT_TRUE(list.IsEmpty());

    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    EXPECT_FALSE(list.IsListEnded());
}

TEST(Datlist, can_ins_first)
{
    TDatList list;
    ASSERT_NO_THROW(list.InsFirst(new TMonom(1, 233)));
    list.InsFirst(new TMonom(9, 541));
    list.InsFirst(new TMonom(3, 393));

    TMonom tmp(3, 393);
    EXPECT_TRUE(tmp == *(TMonom*)list.GetDatValue(FIRST));
    EXPECT_TRUE(*(TMonom*)list.GetDatValue() == *(TMonom*)list.GetDatValue());
}

TEST(Datlist, can_ins_current)
{
    TDatList list;
    list.InsLast(new TMonom(1, 233));
    list.InsLast(new TMonom(9, 541));
    list.InsLast(new TMonom(3, 393));

    list.SetCurrentPos(1);
    ASSERT_NO_THROW(list.InsCurrent(new TMonom(7, 754)));

    TMonom tmp(7, 754);
    list.SetCurrentPos(1);

    EXPECT_TRUE(tmp == *(TMonom*)list.GetDatValue());
}

TEST(Datlist, can_ins_last)
{
    TDatList list;
    ASSERT_NO_THROW(list.InsLast(new TMonom(1, 233)));
    list.InsLast(new TMonom(9, 541));
    list.InsLast(new TMonom(3, 393));

    TMonom tmp(1, 233);
    EXPECT_TRUE(tmp == *(TMonom*)list.GetDatValue());
}

TEST(Datlist, can_del_first)
{
    TDatList list;
    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    list.DelFirst();

    TMonom tmp(2, 242);
    EXPECT_TRUE(*(TMonom*)list.GetDatValue() == tmp);
}

TEST(Datlist, can_del_current)
{
    TDatList list;
    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    list.SetCurrentPos(3);
    list.DelFirst();

    TMonom tmp(4, 464);
    EXPECT_TRUE(*(TMonom*)list.GetDatValue() == tmp);
}

TEST(Datlist, can_del_list)
{
    TDatList list;
    int listLen = 7;
    for (int i = 1; i <= listLen; i++)
        list.InsLast(new TMonom(i, i * 100 + (i + 2) * 10 + i));
    list.DelList();

    EXPECT_TRUE(list.IsEmpty());
}

TEST(Monom, can_create_monom)
{
    ASSERT_NO_THROW(TMonom m(11, 222));
}

TEST(Monom, can_set_and_get_index)
{
    TMonom m(11, 222);
    ASSERT_NO_THROW(m.SetIndex(6));
    EXPECT_EQ(m.GetIndex(), 6);
}

TEST(Monom, can_set_and_get_coef)
{
    TMonom m(11, 222);
    ASSERT_NO_THROW(m.SetCoeff(6));
    EXPECT_EQ(m.GetCoeff(), 6);
}

TEST(Monom, two_different_monoms_are_not_equal)
{
    TMonom m1(111, 222);
    TMonom m2(222, 111);
    EXPECT_FALSE(m1 == m2);
}

TEST(Monom, two_same_monoms_are_equal)
{
    TMonom m1(111, 222);
    TMonom m2(m1);
    EXPECT_TRUE(m1 == m2);
}

TEST(Monom, operator_less_works_correctly)
{
    TMonom m1(111, 222);
    TMonom m2(222, 111);
    EXPECT_TRUE(m2 < m1);
    EXPECT_FALSE(m1 < m2);
}


TEST(Polynom, can_create_polinom)
{
    int monoms[2][2] = { { 5, 945 },{ 7, 843 } };
    ASSERT_NO_THROW(TPolynom p(monoms, 2));
}

TEST(Polynom, can_create_empty_polinom)
{
    ASSERT_NO_THROW(TPolynom p);
}

TEST(Polynom, cant_create_polinom_with_negative_number_of_monoms)
{
    int monoms[2][2] = { { 5, 945 },{ 7, 843 } };
    ASSERT_ANY_THROW(TPolynom p(monoms, -5));
}

TEST(Polynom, can_copy_polinom)
{
    int monoms[2][2] = { { 5, 945 },{ 7, 843 } };
    TPolynom p1(monoms, 2), p2(p1);
    p1.Reset(); p2.Reset();

    for (; !p1.IsListEnded(); p1.GoNext(), p2.GoNext())
    {
        EXPECT_EQ(p1.GetMonom()->GetCoeff(), p2.GetMonom()->GetCoeff());
        EXPECT_EQ(p1.GetMonom()->GetIndex(), p2.GetMonom()->GetIndex());
    }
}

TEST(Polynom, copied_polinom_has_its_own_memory)
{
    int monoms[2][2] = { { 5, 945 },{ 7, 843 } };
    TPolynom p1(monoms, 2), p2(p1);
    p1.Reset(); p2.Reset();
    p1.GetMonom()->SetCoeff(1);
    p1.GetMonom()->SetIndex(111);

    EXPECT_NE(p1.GetMonom()->GetCoeff(), p2.GetMonom()->GetCoeff());
    EXPECT_NE(p1.GetMonom()->GetIndex(), p2.GetMonom()->GetIndex());
}

TEST(Polynom, can_equate_polinoms)
{
    int monoms1[2][2] = { { 5, 945 },{ 7, 843 } };
    int monoms2[4][2] = { { 7, 345 },{ 2, 632 },{ 4, 464 },{ 3, 452 } };

    TPolynom p1(monoms1, 2), p2(monoms2, 4);

    p1 = p2;
    p1.Reset(); p2.Reset();

    for (; !p1.IsListEnded(); p1.GoNext(), p2.GoNext())
    {
        EXPECT_EQ(p1.GetMonom()->GetCoeff(), p2.GetMonom()->GetCoeff());
        EXPECT_EQ(p1.GetMonom()->GetIndex(), p2.GetMonom()->GetIndex());
    }
}

TEST(Polynom, equal_polinom_have_different_memory)
{
    int monoms1[2][2] = { { 5, 945 },{ 7, 843 } };
    int monoms2[4][2] = { { 7, 345 },{ 2, 632 },{ 4, 464 },{ 3, 452 } };

    TPolynom p1(monoms1, 2), p2(monoms2, 4);

    p1 = p2;
    p1.Reset(); p2.Reset();

    p1.GetMonom()->SetCoeff(1);
    p1.GetMonom()->SetIndex(111);

    EXPECT_NE(p1.GetMonom()->GetCoeff(), p2.GetMonom()->GetCoeff());
    EXPECT_NE(p1.GetMonom()->GetIndex(), p2.GetMonom()->GetIndex());
}


TEST(Monom, can_get_data_value)
{
    int monoms[1][2] = { 5,555 };
    TPolynom poly(monoms, 1);
    int g = poly.GetMonom()->GetIndex();
    EXPECT_EQ(555, g);
}