#include "HeadRing.h"

using namespace std;

typedef TMonom *PTMonom;
class TPolynom : public THeadRing {
public:
    void sort(int monoms[][2], int km);
    TPolynom(int monoms[][2] = nullptr, int km = 0); // конструктор
    TPolynom(const TPolynom &q);      // конструктор копирования
    PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
    TPolynom operator+(TPolynom &q); // сложение полиномов
    TPolynom & operator=(const TPolynom &q); // присваивание
};