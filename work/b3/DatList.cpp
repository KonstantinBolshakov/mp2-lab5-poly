#include "DatList.h"
#define OUT_OF_RANGE "Out of range"


TDatList::TDatList() {
    pFirst = pLast = pCurrLink = pPrevLink = pStop = nullptr;
    ListLen = 0;
    Reset();
}


PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
    return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)
{
    if (pLink != nullptr)
    {
        if (pLink->pValue != nullptr)
            delete pLink->pValue;
        delete pLink;
    }
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
    return (mode == FIRST) ? (pFirst->pValue) : (mode == LAST ? pLast->pValue : pCurrLink->pValue);
}

int TDatList::SetCurrentPos(int pos)
{
    if (pos < 0 || pos >= ListLen)
        throw OUT_OF_RANGE;
    else
    {
        Reset();
        for (int i = 0; i < pos; i++)
            GoNext();
        CurrPos = pos;
        return 1;
    }
}


int TDatList::Reset()
{
    if (IsEmpty())
        return -1;
    else
        pPrevLink = pStop,
        pCurrLink = pFirst,
        CurrPos = 0;
    return 1;
}

int TDatList::GoNext()
{
    if (IsEmpty())
        return -1;
    else
    {
        pPrevLink = pCurrLink;
        if (!IsListEnded()) {
            pCurrLink = pCurrLink->GetNextDatLink(),
                CurrPos++;
        }
    }
    return 1;
}

void TDatList::InsFirst(PTDatValue pVal)
{
    PTDatLink tmp = GetLink(pVal, pFirst);
    pFirst = tmp; ListLen++;
    if (pLast == pStop)
        pLast = pCurrLink = tmp,
        CurrPos = 0;
    else if (CurrPos == 0)
        pPrevLink = pFirst,
        CurrPos = 1;
    else CurrPos++;
}

void TDatList::InsLast(PTDatValue pVal)
{
    PTDatLink tmp = GetLink(pVal, pStop);
    PTDatLink last = pLast;
    pLast = tmp; ListLen++;
    if (pFirst == pStop)
        pFirst = pCurrLink = tmp,
        CurrPos = 0;
    else last->SetNextLink(pLast);
    if (IsListEnded())pCurrLink = pLast;
}

void TDatList::InsCurrent(PTDatValue pVal)
{
    if (IsEmpty() || CurrPos == 0) InsFirst(pVal);
    else if (IsListEnded()) InsLast(pVal);
    else {
        PTDatLink tmp = GetLink(pVal, pCurrLink);
        pPrevLink->SetNextLink(tmp);
        pPrevLink = tmp;
        CurrPos++;
        ListLen++;
    }
}

void TDatList::DelFirst()
{
    if (!IsEmpty())
    {
        PTDatLink first = pFirst;
        pFirst = pFirst->GetNextDatLink();
        ListLen--; DelLink(first);
        if (IsEmpty())
            pPrevLink = pCurrLink = pFirst = pLast = pStop;
        else if (CurrPos == 0) pCurrLink = pFirst;
        else if (CurrPos == 1) pPrevLink = pStop;
        else CurrPos--;

    }
}

void TDatList::DelCurrent()
{
    if (!IsEmpty())
    {
        if (pCurrLink == pFirst)DelFirst();
        else
        {
            PTDatLink current = pCurrLink;
            pCurrLink = pCurrLink->GetNextDatLink();
            pPrevLink->SetNextLink(pCurrLink);
            if (current == pLast) pLast = pPrevLink;
            DelLink(current); ListLen--;
        }
    }
}

void TDatList::DelList()
{
    while (!IsEmpty()) DelFirst();
    pPrevLink = pCurrLink = pFirst = pLast = pStop;
}